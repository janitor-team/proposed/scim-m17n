%define nam             scim-m17n
%define ver             0.2.3
%define rel             1

# Something's not quite right with libtool....
%define __libtoolize    echo

Summary:        M17N Input Method Engine for SCIM
Name:           %{nam}
Version:        %{ver}
Release:        %{rel}
License:        GPL
Group:          System Environment/Libraries
URL:            http://www.freedesktop.org/~suzhe
BuildRoot:      %{_tmppath}/%{name}-%{version}-root

Source0:        %{name}-%{version}.tar.gz
#NoSource:	0

PreReq:         /sbin/ldconfig, /bin/sh

Requires:	scim >= 1.4.0
BuildRequires:  scim-devel >= 1.4.0
Requires:	m17n-lib >= 1.2.0
BuildRequires:  m17n-lib-devel >= 1.2.0

%description
SCIM is a developing platform to significant reduce the difficulty of 
input method development. 

%changelog
* Sun Jun 13 2004 James Su <suzhe@tsinghua.org.cn>
- first release of scim-m17n.

#--------------------------------------------------

%prep

%setup -n %{name}-%{version}

%build
%configure --disable-static

make 

%install
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT
make DESTDIR=${RPM_BUILD_ROOT} install

rm -f $RPM_BUILD_ROOT//usr/lib/scim-1.0/1.4.0/IMEngine/m17n.{a,la}

%clean
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)
%doc AUTHORS COPYING NEWS README ChangeLog
/usr/lib/scim-1.0/1.4.0/IMEngine/m17n.so
/usr/share/scim/icons/scim-m17n.png
